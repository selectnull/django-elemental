clean:
	rm -rf dist/ build/
	find . -name "*.pyc" -delete
	find . -name "*.orig" -delete

sdist:
	python setup.py sdist
