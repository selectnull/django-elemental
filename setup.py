from distutils.core import setup


version = __import__('elemental').get_version()

setup(
    name='django-elemental',
    version=version,
    packages=['elemental', 'elemental.core', 'elemental.cms', 'elemental.cms.models', ],
    license='MIT',
    long_description=open('README').read(),
)
