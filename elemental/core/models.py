from django.db import models
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.generic import GenericForeignKey
from django.utils.translation import ugettext_lazy as _


class Element(models.Model):
    # content object fields
    content_type = models.ForeignKey(ContentType,
        related_name='content_type_set_for_%(class)%',
        verbose_name=_('content_type')
    )
    object_id = models.IntegerField(verbose_name=_('Object ID'))
    content_object = GenericForeignKey(ct_field="content_type", fk_field="object_id")

    group = models.CharField(max_length=50, blank=True,
        verbose_name=_(u'Group'))
    position = models.IntegerField(blank=True,
        verbose_name=_(u'Position'))
    enabled = models.BooleanField(default=True,
        verbose_name=_(u'Enabled'))

    content = models.TextField(blank=True,
        verbose_name=_(u'Content'))

    comment = models.CharField(max_length=100, blank=True,
        verbose_name=_(u'Comment'))

    class Meta:
        app_label = 'core'
        db_table = 'elemental_elements'
        verbose_name = _(u'Element')
        verbose_name_plural =_(u'Elements')

    def __unicode__(self):
        return u'{} ({})'.format(self.group, self.position)
