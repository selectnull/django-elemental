from django.contrib import contenttypes

from .models import Element


class ElementInline(contenttypes.generic.GenericStackedInline):
    model = Element
    extra = 0
    ct_field = 'content_type'
    fk_field = 'object_id'

    fieldsets = (
        (None, {
            'fields': (
                ('group', 'enabled', 'position', ),
                ('content', ),
                ('comment', )
            ),
        }),
    )
