from django.conf import settings


def get_setting_or_default(setting_name, default_value=None):
    try:
        return getattr(settings, setting_name)
    except:
        return default_value


def get_languages():
    return get_setting_or_default('LANGUAGES', ())

def is_multilingual():
    return len(get_languages()) > 1

def get_default_language():
    return get_languages()[0][0]

def get_page_templates():
    return get_setting_or_default('CMS_TEMPLATES', ())

def get_all_parent_urls():
    return ()
