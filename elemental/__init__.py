""" django-elemental """


VERSION = (0, 1, 1, 'dev')

def get_version():
    version = '{}.{}'.format(VERSION[0], VERSION[1])
    if VERSION[2]:
        version = '{}.{}'.format(version, VERSION[2])
    if VERSION[3]:
        version = '{}-{}'.format(version, VERSION[3])
    return version
