from django.http import Http404
from django.shortcuts import render
from django.contrib.auth import get_user_model

from .models import Page
from ..utils import get_page_templates


def page(request):
    try:
        page = Page.objects.get(url=request.path)
    except Page.DoesNotExist:
        raise Http404

    if not page.is_published:
        if not request.user.is_staff or not request.user == page.published_by:
            raise Http404

    template_name = page.template_name
    if not template_name:
        template_name = get_page_templates()[0][0]
    return render(request, template_name, {'page': page})
