from django.contrib import admin

from .models import Menu, MenuItem, Page
from ..core.admin import ElementInline
from ..utils import get_languages, get_page_templates


class MenuItemInline(admin.TabularInline):
    model = MenuItem
    extra = 0

class MenuAdmin(admin.ModelAdmin):
    inlines = (MenuItemInline, )


class PageAdmin(admin.ModelAdmin):
    list_display = ('title', 'url', )
    search_fields = ('title', 'url', 'elements__content', )
    list_filter = ('author', )
    prepopulated_fields = {'url': ('title', )}

    fieldsets = (
        (None, {
            'fields': ('title', 'url', )
        }),
        ('Advanced settings', {
            'fields': (
                'is_published',
                ('author', 'published_on', ),
                'template_name', 'code',
            )
        }),
    )

    if len(get_languages()) > 1:
        list_display += ('language', )
        list_filter += ('language', )
        fieldsets[0][1]['fields'] += ('language', )

    inlines = (ElementInline, )


admin.site.register(Menu, MenuAdmin)
admin.site.register(Page, PageAdmin)
