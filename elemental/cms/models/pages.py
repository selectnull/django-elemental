from django.db import models
from django.conf import settings

from ...utils import get_page_templates, get_all_parent_urls, get_languages, get_default_language
from ...core.models import Element
from django.contrib.contenttypes.generic import GenericRelation

from django.utils.translation import ugettext_lazy as _
import datetime


class Page(models.Model):
    title = models.CharField(max_length=200,
        verbose_name=_(u'Title'))
    url = models.CharField(max_length=200, unique=True,
        verbose_name=_(u'Url'))

    template_name = models.CharField(max_length=50, blank=True, choices=get_page_templates(),
        verbose_name=_(u'Template'))

    language = models.CharField(max_length=2,
        choices=get_languages(), default=get_default_language(),
        verbose_name=_(u'Language'))

    # advanced fields
    is_published = models.BooleanField(default=True)
    published_on = models.DateTimeField(default=datetime.datetime.now)
    author = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, blank=True)

    code = models.CharField(max_length=200, blank=True,
        verbose_name=_(u'Code'))

    # seo fields
    meta_description = models.TextField(blank=True,
        verbose_name=_(u'Meta description'))

    # content elements
    elements = GenericRelation(Element)
    
    class Meta:
        app_label = 'cms'
        db_table = 'elemental_pages'
        verbose_name = _(u'Page')
        verbose_name_plural = _(u'Pages')
        ordering = ('url', )

    def __unicode__(self):
        return self.title

    def get_absolute_url(self):
        return self.url

    def get_breadcrumbs(self):
        args = {'language': self.language, 'url__in': get_all_parent_urls(self.url)}
        return Page.objects.filter(**args).order_by('url')
