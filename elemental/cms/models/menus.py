from django.db import models
from django.utils.translation import ugettext_lazy as _

from ...utils import get_languages, get_default_language


class Menu(models.Model):
    code = models.CharField(max_length=40,
        verbose_name=_(u'Code'))
    title = models.CharField(max_length=100, blank=True,
        verbose_name=_(u'Title'))
    language = models.CharField(max_length=2,
        choices=get_languages(), default=get_default_language(),
        verbose_name=_(u'Language'))

    class Meta:
        app_label = 'cms'
        db_table = 'elemental_menus'
        verbose_name = _(u'Menu')
        verbose_name_plural = _(u'Menus')

        unique_together = ('language', 'code', )

    def __unicode__(self):
        return self.code

    def items(self):
        return self.all_items.filter(enabled=True)


class MenuItem(models.Model):
    menu = models.ForeignKey(Menu, related_name='all_items')

    title = models.CharField(max_length=400,
        verbose_name=_(u'Title'))
    url = models.CharField(max_length=400,
        verbose_name=_(u'Url'))

    position = models.CharField(max_length=40,
        verbose_name=_(u'Position'))
    enabled = models.BooleanField(default=True,
        verbose_name=_(u'Enabled'))

    class Meta:
        app_label = 'cms'
        db_table = 'elemental_menu_items'
        verbose_name = _(u'Menu item')
        verbose_name_plural = _(u'Menu items')
        ordering = ('position', )

    def __unicode__(self):
        return self.title
